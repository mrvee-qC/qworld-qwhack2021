---
name: Submission Template
about: Enter the hackathon with a project
title: Project template
labels: ''
assignees: ''

---
# Category: Problem Statement 1 <!-- choose which statement you wish to work on. -->

# Description
<-A few paragraphs describing the projects. Projects should be completed within the stipulated timelines. The projects will be expected to demostrate technical skills and; 2) Tackle problems that relate to quantum computing within the topics and themese mentioned in the hackathon page->

# Mentor/s
<-If needed->

# Type of participants required
<-What are the profiles of the ideal participants for this idea? (Eg: Programmers, Physicists, etc describe what criteria you are looking for in your team)->

# Number of participants
<-What is the number of participants you are willing to take in this project? (Should not exceed the maximum limit specified in the rules)?->
    
  
# Deliverable
<-What's the expected result?->




# Repository: 
https://github.com/random/hackathon-submission <!-- indicate the GitHub repository with the submission -->


# Members: <!-- up to 6 members per term -->
- **Abu**, <u>Abu(He/Him)</u>, Faculty of Computing, University of Latvia
- **Vishal Bajpe**, <u>Vishal_Bajpe(He/Him)</u>, Sahyadri College of Engineering & Management

 <!-- Indicate full name, Discord username, institution and program, if you are an student -->
