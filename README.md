<img src="https://qworld.net/wp-content/uploads/2019/10/cropped-logo_top-1.jpg">

# QWorld QWHACK 2021

There is a growing interest in this world, regarding the rapidly developing field of quantum technologies in the world. QWHACK 2021 brings together enthusiasts, experts and academics who are interested in quantum technologies. In this context, we expanded our Hackathon, which we will organize as QWorld, to cover all quantum technologies and sub-sections this year.

Teams can develop ideas, projects and applications in quantum simulation, quantum computing, quantum sensing, quantum communication, and beyond. You can join Hackathon individually or with your team. Individual applications will be directed to form suitable groups before hackathon.

Awards will be given in 3 main categories in the Hackathon:

Academic / Scientific Studies
Industrial / Technological Solutions
Training and Education Materials

Teams participating in the Hackathon will submit their ideas, projects or applications to be evaluated under one of these categories. The presentations made by the teams in each category will be evaluated by the jury members who are experts in their fields.

## Proposed task

## How to participate

##  Sample problem statemes

## Code of Conduct

## Discord Server

## Categories & prizes

## Projects & deliverables

## Submitting & submission deadline

##  Team formation

##  Project report

## Evaluation criteria

## Learning materials

##  References
